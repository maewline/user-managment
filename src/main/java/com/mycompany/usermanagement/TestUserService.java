/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanagement;

/**
 *
 * @author Arthi
 */
public class TestUserService {

    public static void main(String[] args) {
        UserService.addUser("Arthit", "12345");
        UserService.addUser(new User("User1", "1234"));
        System.out.println(UserService.getUsers());

        User user = UserService.getUser(3);
        System.out.println(user);
        user.setPassword("5555");
        UserService.updateUser(3, user);
        System.out.println(UserService.getUsers());

        System.out.println(UserService.getUsers());
        System.out.println(UserService.login("Arthit", "12345"));

    }
}
